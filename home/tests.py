from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
import unittest
# Create your tests here.

class IndexUnitTest(TestCase):
    def test_index_url_is_exist(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 200)
    
    def test_index_html_text(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('STUDENT UNION.', html_response)
        self.assertIn('learn today, lead tomorrow', html_response)