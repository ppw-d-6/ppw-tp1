from django.shortcuts import render, redirect
from django.contrib.auth import logout
from django.http import HttpResponseRedirect
# Create your views here.


def index(request):
    return render(request, 'index.html')
    
def logoutPage(request):
	request.session.flush()
	logout(request)
	return HttpResponseRedirect('/home/')

def indexlogin(request):
    if  request.user.is_authenticated :
        request.session['sessionkey'] = request.session.session_key
        request.session['email'] = request.user.email
        request.session['username'] = request.user.username
        request.session['fullname'] = request.user.first_name + " " + request.user.last_name
        request.session.modified = True
        print(request.session.items())
    return render(request,'indexlogin.html')







