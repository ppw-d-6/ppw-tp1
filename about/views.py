from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .forms import TestimonialForm
from .models import Testimonial
import requests, json


# Create your views here.
response = {}
@csrf_exempt
def post(request):
    if (request.method == "POST"):
        message = request.POST.get('the_post')
        if request.user.is_authenticated:
            if 'name' not in request.session:
                request.session['name'] = request.user.first_name + " " + request.user.last_name
        response['name'] = request.session['name']
        name = response['name']
        new = Testimonial.objects.create(name=name, testimonial=message)
        print(new.date)
        time = new.date.strftime("%b. %d, %Y")
        response['testimonial'] = message
        response['time'] = time
        return HttpResponse(json.dumps(response), content_type="application/json")
    else:
        return HttpResponse(json.dumps({"nothing": "failed"}), content_type="application/json")


def testimonials(request):
    response = {}
    testimonials = Testimonial.objects.all()
    data = TestimonialForm()
    return render(request, "about.html", {'form': data, 'testimonials': testimonials})