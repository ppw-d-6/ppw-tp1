from django import forms
from .models import Testimonial

class TestimonialForm(forms.Form):
    attrs = {
        'class':'form-control',
        'id': 'post-text'
    }

    testimonial = forms.CharField(required=True, max_length=300, label='testimonial', widget=forms.Textarea(attrs=attrs))