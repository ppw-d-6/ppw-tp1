from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Testimonial
from .views import testimonials
from .views import post
from .forms import TestimonialForm
import unittest
# Create your tests here.
class TP2UnitTest(TestCase):
    def test_about_url_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_logout_about_url_exist(self):
        response = Client().get('/about/post/')
        self.assertEqual(response.status_code, 200)

    def test_about_using_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')

    def test_about_using_create_form_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, testimonials)

    def test_model_can_create_new_testimonial(self):
        new = Testimonial.objects.create(testimonial="test create new testimonial")
        
        count = Testimonial.objects.all().count()
        self.assertEqual(count, 1)
    
    def test_about_html_text(self):
        request = HttpRequest()
        response = testimonials(request)
        html_response = response.content.decode('utf8')
        self.assertIn('ABOUT', html_response)