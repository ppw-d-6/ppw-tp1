from django.urls import path
from . import views

app_name = 'about'
urlpatterns = [
    path('', views.testimonials, name='testimonials'),
    path('post/', views.post, name='post'),
]