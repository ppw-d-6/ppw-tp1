from django.db import models

# Create your models here.
class Testimonial(models.Model):
    name = models.CharField(max_length=200, blank=True)
    testimonial = models.CharField(max_length=300)
    date = models.DateField(auto_now_add=True)
