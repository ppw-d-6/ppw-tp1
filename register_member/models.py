from django.db import models
from datetime import datetime, date

class DaftarMember(models.Model):
    nama = models.CharField(max_length=300)
    username = models.CharField(max_length=100, unique = True)
    email = models.CharField(max_length=254, unique = True)
    tglLahir = models.DateField()
    password = models.CharField(max_length=50)
    alamat = models.CharField(max_length=300)



# Create your models here.
