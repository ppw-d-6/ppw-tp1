from django import forms

class Message_Form(forms.Form):
    attrs = {
        'class' : 'form-control'
    }
    nama = forms.CharField(max_length = 300, label = "Nama Lengkap", widget = forms.TextInput(attrs=attrs))
    username = forms.CharField(max_length=100, label = "Username", widget = forms.TextInput(attrs=attrs))
    email = forms.CharField(max_length=254 , label = "Email", widget=forms.TextInput(attrs={'class':'form-control', 'type':'email'}))
    tglLahir = forms.DateField(label = "Tanggal Lahir", widget=forms.DateInput(attrs={'class':'form-control', 'type' : 'date'}))
    password = forms.CharField(max_length=50, widget = forms.PasswordInput(attrs=attrs), label = "Password")
    alamat = forms.CharField(label = "Alamat",  widget = forms.TextInput(attrs=attrs))