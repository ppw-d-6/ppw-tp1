from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *

class FormUnitTest(TestCase):
    def test_root_url_is_exist(self):
        response = Client().get('/register_member/Daftar')
        self.assertEqual(response.status_code,200)

    def test_submit_url_is_exist(self):
        response = Client().get('/register_member/User')
        self.assertEqual(response.status_code,200)


    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = formsubmit(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Register Member', html_response)

    def test_form_is_valid(self):
        form = Message_Form(data = {'nama' : 'Oristania Wahyu Nabasya' , 'username' : 'nasyaoris', 'email' : '' , 'tglLahir' : '' , 'password' : 'nasyanada9' , 'alamat' : 'jalan Pinang Kalijati'})
        self.assertFalse(form.is_valid())

    def test_model_can_create_new_message(self):
        new_activity = DaftarMember.objects.create(nama= 'Oristania Wahyu Nabasya', username= 'nasyaoris' ,email='test@gmail.com', tglLahir = '1999-03-09', password = 'nasyanada9', alamat = 'jalan pinang kalijati')
        counting_all_available_message= DaftarMember.objects.all().count()
        self.assertEqual(counting_all_available_message,1)

    def test_login_google(self):
        request = HttpRequest()
        response = formsubmit(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Login with google", html_response)