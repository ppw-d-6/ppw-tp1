from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import Message_Form
from .models import DaftarMember
from django.http import HttpResponse
import pytz
from datetime import datetime
from django.db import IntegrityError
from django.contrib.auth import logout

def formsubmit(request):
    response = {}
    response['message_form'] = Message_Form
    daftar = DaftarMember.objects.all()
    response['daftar'] = daftar
    return render(request, 'formDaftar.html', response)

def message_post(request):
    response = {}
    form = Message_Form(request.POST or None)
    try:
        if(request.method == "POST"):
            response["nama"] = request.POST["nama"]
            response["username"] = request.POST["username"]
            response["tglLahir"] = datetime.strptime(request.POST["tglLahir"], '%Y-%m-%d')
            response["email"] = request.POST["email"]
            response["password"] = request.POST["password"]
            response["alamat"] = request.POST["alamat"]

            daftar = DaftarMember(nama = response["nama"], username = response["username"], tglLahir = response["tglLahir"], email = response["email"], password = response["password"], alamat = response["alamat"])
            daftar.save()
            response['daftar'] = DaftarMember.objects.all().values()
            html = "thanks.html"
            return render(request, html, response)
        
        else:
            return HttpResponseRedirect('Daftar')
    except IntegrityError as e:
        return HttpResponseRedirect('Daftar')

def daftar_user(request):
    response = {}
    daftar = DaftarMember.objects.all()
    response["daftar_user"] = daftar
    html = "daftarUser.html"
    return render(request, html , response)
# Create your views here.
