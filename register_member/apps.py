from django.apps import AppConfig


class RegisterMemberConfig(AppConfig):
    name = 'register_member'
