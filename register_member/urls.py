from django.conf.urls import url, include
from django.contrib import admin
from .views import *

app_name = 'register_member'
urlpatterns = [
    url(r'^User', daftar_user, name='daftar_user'),
    url(r'^Register', message_post, name='message_post'),
    url(r'^Daftar', formsubmit, name='formsubmit'),
    url(r'^auth/', include('social_django.urls', namespace='social'))
]
