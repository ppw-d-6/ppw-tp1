# Tugas Pemrograman Perancangan dan Pemrograman Web Kelas D Kelompok 6

## Link Heroku
https://ppw-6-d.herokuapp.com/

## Anggota Kelompok dan Pembagian Tugas:
Tugas Pemrograman 1:
1. Alya Zahra - 1706039906 (News)
2. Oristania Wahyu Nabasya - 1706025390 (Register Member)
3. Putri Kinanti Ramadhani Birowo - 1706022810 (Home dan Register Event)
4. Ray Azrin Karim - 1706044111 (Events)


Tugas Pemrograman 2:
1. Alya Zahra - 1706039906 (List Event yang Sudah Didaftarkan [no. 4])
2. Oristania Wahyu Nabasya - 1706025390 (Login dengan Google [no. 1])
3. Putri Kinanti Ramadhani Birowo - 1706022810 (About dan Testimoni [no. 5 & 6])
4. Ray Azrin Karim - 1706044111 (Daftar List Event tanpa Login dan Daftar Event dengan Login [no. 2 & 3])

`* Nomor yang dimaksud adalah nomor pada list halaman yang perlu dikerjakan pada Panduan Tugas 2`

## pipeline
[![pipeline status](https://gitlab.com/ppw-d-6/ppw-tp1/badges/master/pipeline.svg)](https://gitlab.com/ppw-d-6/ppw-tp1/commits/master)

## coverage
[![coverage report](https://gitlab.com/ppw-d-6/ppw-tp1/badges/master/coverage.svg)](https://gitlab.com/ppw-d-6/ppw-tp1/commits/master)
