from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from events.models import Events
from django.contrib.auth.models import User
import time

class EventListUnitTest(TestCase):
	def test_eventlist_unlogged(self):
		response = Client().get('/registeredEvent/')
		self.assertEqual(response.status_code, 302)
	   
	def test_eventlist_use_exist_and_use_template(self):
		user = User.objects.create(username='alzahra')
		user.set_password('AlyaZahra22')
		user.save()
		login = self.client.login(username='alzahra', password='AlyaZahra22')
		response = self.client.get('/registeredEvent/')
		self.assertTemplateUsed(response, 'registeredEvent.html')

	def test_eventlist_use_index(self):
		user = User.objects.create(username='alzahra')
		user.set_password('AlyaZahra22')
		user.save()
		login = self.client.login(username='alzahra', password='AlyaZahra22')
		found = resolve('/registeredEvent/')
		self.assertEqual(found.func, index)

	def test_eventlist_content(self):
		user = User.objects.create(username='alzahra')
		user.set_password('AlyaZahra22')
		user.save()
		login = self.client.login(username='alzahra', password='AlyaZahra22')
		new_response = self.client.get('/registeredEvent/')
		html_response = new_response.content.decode('utf-8')
		self.assertIn('LIST EVENT', html_response)

	def test_register_event(self):
		event = Events.objects.create(title='EVENT UI', description='Test')
		event.participants.add(Register.objects.create(user='alzahra', password="AlyaZahra22", email="alya@gmail.com"))
		user = User.objects.create(username='alzahra')
		user.set_password('AlyaZahra22')
		user.save()
		login = self.client.login(username='alzahra', password='AlyaZahra22')
		response = self.client.get('/registeredEvent/getRegisteredEvent')
		html_response = response.content.decode('utf-8')
		self.assertIn('EVENT UI', html_response)