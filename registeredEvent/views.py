from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.urls import reverse 
import json
import requests
from register_event.models import Register
from events.models import Events

def index(request):
	response = {}
	if request.user.is_authenticated:
		html = 'registeredEvent.html'
		myevents = list(Events.objects.filter(participants__user=request.user.username).values
			('title', 'date').order_by('date'))
		response['eventNum'] = len(myevents)
		return render(request, html, response)
	else:
		return HttpResponseRedirect('/../register_member/Daftar')

def getRegisteredEvent(request):
	myevents = list(Events.objects.filter(participants__user=request.user.username).values('title', 'date').order_by('date'))
	for i in myevents:
		i["date"] = i["date"].date()
	return JsonResponse({'registeredEvent' : myevents})