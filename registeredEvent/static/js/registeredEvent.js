$(document).ready(function(){
	showEvents();

	function showEvents(){
		$(function() {
			$.ajax({
				type: 'GET',
				url: 'getRegisteredEvent',
				dataType: 'json',
				success: function(result){
					$('#eventlist').empty();
					$.each(result.registeredEvent, function(i, e) {
						$('<tr>').append(
							$('<td>').text(i + 1),
							$('<td>').text(e.title),
							$('<td>').text(e.date),
						).appendTo('#eventlist');
				    });
				}
			});
		});
	}
});