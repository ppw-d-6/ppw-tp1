from django.urls import path
from . import views
from .views import *

app_name = 'registeredEvent'
urlpatterns = [
    path('', index, name='index'),
    path('getRegisteredEvent', getRegisteredEvent, name='getRegisteredEvent'),
]