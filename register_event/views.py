from django.shortcuts import render
# from django.http import HttpResponse
# from .forms import Register_Form
from .models import Register
from events.models import Events
from django.shortcuts import redirect
from django.db import IntegrityError

response = {}


# def register_event(request, pk):
#     form = Register_Form(request.POST or None)
#     event = Events.objects.get(pk=pk)
#     registered = Register.objects.all()
#     response = {
#         "registered": registered,
#         "event": event
#     }
#     response['form'] = form
#
#     try:
#         if(request.method == "POST" and form.is_valid()):
#             user = request.user.username
#             password = request.user.password
#             email = request.user.email
#             event.participants.add(Register.objects.create(
#                 user=user, password=password, email=email, event=event))
#             return redirect('register_event:thankyou')
#         else:
#             return render(request, 'register_event.html', response)
#     except IntegrityError as e:
# return render(request, 'register_event.html', response)

def register_event(request, pk):
    event = Events.objects.get(pk=pk)
    response['event'] = event
    return render(request, 'register_event.html', response)


def thankyou(request, pk):
    event = Events.objects.get(pk=pk)
    email = request.user.email
    if event.participants.filter(email=email).exists():
        return render(request, 'alr_registered.html')
    else:
        user = request.user.username
        password = request.user.password
        event = Events.objects.get(pk=pk)
        event.participants.add(Register.objects.create(
            user=user, password=password, email=email))
        response['event'] = event

    return render(request, 'thankyou.html', response)
