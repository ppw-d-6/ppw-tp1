from django import forms
from .models import Register
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError

class Register_Form(forms.Form):

    user_attrs = {
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'username should be unique',
    }

    password_attrs = {
        'type' : 'password',
        'class' : 'todo-form-input',
        'placeholder' : 'combination of alphabets and numbers',
    }

    email_attrs = {
        'type' : 'email',
        'class' : 'todo-form-input',
        'placeholder' : 'email should be unique',
    }

    user = forms.CharField(label='username', max_length=20, required=True, widget= forms.TextInput(attrs=user_attrs), error_messages={'invalid': 'username is not unique!'})
    password = forms.CharField(label='password',validators=[RegexValidator('^(\w+\d+|\d+\w+)+$', message="must contain a combination of alphabets and numbers")],
                                max_length=30, min_length=8, required=True, widget=forms.PasswordInput(attrs=password_attrs))
    email = forms.EmailField(label='email', required=True, widget= forms.TextInput(attrs=email_attrs), error_messages={'invalid': 'email is not unique!'})
