from django.db import models
from datetime import datetime, date

# Create your models here.


class Register(models.Model):
    user = models.CharField(max_length=20)
    password = models.CharField(max_length=100)
    email = models.EmailField(null=True)
