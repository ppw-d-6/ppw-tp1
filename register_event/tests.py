from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Register
from events.models import Events
from .views import register_event, thankyou
from .forms import Register_Form
import unittest

# Create your tests here.


class RegisterEventUnitTest(TestCase):
    def test_register_event_exist(self):
        event = Events.objects.create(title='Test', description='Test')
        response = Client().get('/events/register/1/')
        self.assertEqual(response.status_code, 200)

    def test_register_event_use_index(self):
        event = Events.objects.create(title='Test', description='Test')
        found = resolve('/events/register/1/')
        self.assertEqual(found.func, register_event)

    def test_model_can_create_new_status(self):
        event = Events.objects.create(title='Test', description='Test')
        usernames = Register.objects.create(user='dibu')
        response_post = Client().post('/register_event/',
                                      {'user': 'dibu', 'password': 'budinamaku', 'email': 'emailbudi@gmail.com'})
        self.assertEqual(response_post.status_code, 404)

        count_all_username = Register.objects.all().count()
        self.assertEqual(count_all_username, 1)
