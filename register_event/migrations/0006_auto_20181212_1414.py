# Generated by Django 2.1.1 on 2018-12-12 07:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('register_event', '0005_auto_20181212_1332'),
    ]

    operations = [
        migrations.AlterField(
            model_name='register',
            name='email',
            field=models.EmailField(max_length=254, null=True),
        ),
    ]
