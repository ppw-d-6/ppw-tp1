"""tp1 URL Configuration 

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
import home.urls as home
import register_member.urls as register_member
import about.urls as about
from django.conf.urls import url


urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', include('home.urls'), name='home'),
    path('events/', include('events.urls'), name='events'),
    path('events/register/',
         include('register_event.urls'), name='register_event'),
    path('news/', include('news.urls'), name='news'),
    path('register_member/',include(register_member), name='register_member'),
    path('about/',include('about.urls'), name='about'),
    url(r'^auth/', include('social_django.urls', namespace='social')),
    path('logout/', home.views.logoutPage, name='logout'),
    path('registeredEvent/', include('registeredEvent.urls'), name='registeredEvent'),
]
