from django.db import models

class News(models.Model):
	title = models.CharField(max_length=100)
	content = models.CharField(max_length=50000)
	date = models.DateTimeField()