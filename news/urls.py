from django.urls import path
from django.conf.urls import url
from . import views
from .views import *

app_name = 'news'
urlpatterns = [
    path('', views.index, name='index'),
    path('view_news', views.view_news, name='view_news'),
]