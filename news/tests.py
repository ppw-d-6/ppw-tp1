from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from .models import News
from .views import *

class NewsTestcase(TestCase):
    def test_news_exist(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code, 200)

    def test_news_use_template(self):
        response = Client().get('/news/')
        self.assertTemplateUsed(response, 'news.html')

    def test_news_use_index(self):
        found = resolve('/news/')
        self.assertEqual(found.func, index)

    def test_news_can_create_news(self):
        new_news = News.objects.create(title='Anime Festival Universitas X Menjadi Populer',
        	content='Dikarenakan naiknya popularitas anime di kalangan mahasiswa, festival anime yang ' +
        	'diselenggarakan Universitas X menjadi populer', date=timezone.now())
        available_news = News.objects.all().count()
        self.assertEqual(available_news, 1)

    def test_html_content(self):
        new_response = self.client.get('/news/')
        html_response = new_response.content.decode('utf-8')
        self.assertIn('NEWS', html_response)

    def test_can_see_news_article(self):
        new_news = News.objects.create(title='news1example', content='lorem ipsum', date=timezone.now())
        new_response = self.client.get('/news/view_news?id=1')
        self.assertTemplateUsed(new_response, 'news_page.html')