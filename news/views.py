from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import *
from django.contrib.auth import logout

response = {}

def index(request):
    newslist = News.objects.all().order_by('-date')
    response['newslist'] = newslist
    html = 'news.html'
    return render(request, html, response)

def view_news(request):
	pk = request.GET.get('id', '')
	news = News.objects.get(pk=pk)
	response = {'news': news}
	html = 'news_page.html'
	return render(request, html, response)