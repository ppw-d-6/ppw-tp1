from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'events'

urlpatterns = [
    path('', views.event_list, name='event_list'),
    path('<int:pk>/', views.participant_list, name='participant_list'),
]
