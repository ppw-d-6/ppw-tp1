from django.test import TestCase, Client
from .models import Events
from register_event.models import Register
from django.urls import resolve
from . import views
from django.http import HttpRequest

# Create your tests here.


class EventUnitTest(TestCase):
    def test_register_event_exist(self):
        response = Client().get('/events/')
        self.assertEqual(response.status_code, 200)

    def test_add_participant_has_participant_name(self):
        event = Events.objects.create(title='EVENT UI', description='Test')
        event.participants.add(Register.objects.create(
            user='participant1', password='123', email='user1232gmail.com'))
        request = HttpRequest()
        response = views.participant_list(request, 1)
        html_response = response.content.decode('utf8')
        self.assertIn('participant1', html_response)

    def test_participant_list_exist(self):
        event = Events.objects.create(title='Test', description='Test')
        response = Client().get('/events/1/')
        self.assertEqual(response.status_code, 200)

    def test_event_list_event_use_views(self):
        found = resolve('/events/')
        self.assertEqual(found.func, views.event_list)

    def test_can_create_event(self):
        event = Events.objects.create(title='Test', description='Test')
        count_all_event = Events.objects.all().count()
        self.assertEqual(count_all_event, 1)

    def test_participant_list_event_use_views(self):
        event = Events.objects.create(title='Test', description='Test')
        found = resolve('/events/1/')
        self.assertEqual(found.func, views.participant_list)
