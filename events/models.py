from django.db import models
from datetime import datetime
from register_event.models import Register
# from django.contrib.auth.models import User

# Create your models here.


class Events(models.Model):
    title = models.CharField(max_length=30)
    date = models.DateTimeField(default=datetime.now)
    description = models.CharField(max_length=3200)
    participants = models.ManyToManyField(Register, blank=True)
