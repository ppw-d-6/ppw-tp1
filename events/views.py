from django.shortcuts import render, redirect
from .models import Events
from django.contrib.auth import logout
from django.http import HttpResponseRedirect

def event_list(request):
    events = Events.objects.all().order_by('date')
    if request.user.is_authenticated:
        lst = []
        for i in events:
            checker = False
            for participant in i.participants.all():
                if request.user.email == participant.email:
                    checker = True
                    break
            lst.append({"event": i, "is_participate": checker})
        response = {'events': lst}
    else:
        lst = []
        for i in events:
            lst.append({"event": i})
        response = {'events': lst}
    return render(request, 'events.html', response)


def participant_list(request, pk):
    event = Events.objects.get(pk=pk)
    response = {'event': event}
    return render(request, 'participant_list.html', response)